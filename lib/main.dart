import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.deepPurple[50],
        ),
      ),

      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );
  }
  static ThemeData appThemeDark()
  {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.blueAccent,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),

      iconTheme: IconThemeData(
        color: Colors.white,
      ),
    );

  }
}


class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme:currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(

        appBar: buildAppBarWidget(),
        body: buildBodyBarWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.app_shortcut_sharp ),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("29°"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("31°"),
    ],
  );
}
Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny ,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("31°"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny ,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("32°"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny ,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("31°"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.sunny ,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("30°"),
    ],
  );
}
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.sunny),
    title: Text("วันนี้"),
    subtitle: Text("30°"),
    trailing: IconButton(
      icon: Icon(Icons.notes_sharp ),
      color: Colors.orangeAccent,
      onPressed: () {},
    ),
  );
}
Widget otherPhoneListTile() {
  return ListTile(
    leading: Icon(Icons.cloud),
    title: Text("พ."),
    subtitle: Text("30°"),
    trailing: IconButton(
      icon: Icon(Icons.notes_sharp ),
      color: Colors.orangeAccent,
      onPressed: () {},
    ),
  );
}
Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.cloud),
    title: Text("พฤ."),
    subtitle: Text("32°"),
      trailing: IconButton(
        icon: Icon(Icons.notes_sharp ),
        color: Colors.orangeAccent,
        onPressed: () {},
      )
  );
}
Widget directListTile() {
  return ListTile(
    leading: Icon(Icons.sunny_snowing),
    title: Text("ศ."),
    subtitle: Text("30°"),
    trailing: IconButton(
      icon: Icon(Icons.notes_sharp),
      color: Colors.orangeAccent,
      onPressed: () {},
    ),
  );
}
AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.white10,

    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),

    actions: <Widget>[
      IconButton(
          onPressed: () {},
          icon: Icon(Icons.star_border),
          color : Colors.black
      )
    ],
  );
}
Widget buildBodyBarWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            height: 310,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(

                  padding: EdgeInsets.all(115.0),
                  child: Text("29 °C ",
                    style: TextStyle(fontSize: 60),

                  ),
                ),

              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child : Theme(
              data : ThemeData(
                iconTheme: IconThemeData(
                    color: Colors.orange
                ),
              ),
              child:  profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          directListTile(),
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    ],
  );
}
Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),

    ],
  );
}
